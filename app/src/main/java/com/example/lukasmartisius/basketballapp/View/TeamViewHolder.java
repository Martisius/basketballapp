package com.example.lukasmartisius.basketballapp.View;

import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class TeamViewHolder extends RecyclerView.ViewHolder {

    private String teamName;
    private String teamDescription;
    private Image teamIcon;

    public TeamViewHolder(@NonNull View itemView, String teamName, String teamDescription, Image teamIcon) {
        super(itemView);
        this.teamName = teamName;
        this.teamDescription = teamDescription;
        this.teamIcon = teamIcon;
    }
}
